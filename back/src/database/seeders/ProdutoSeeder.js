const Produto = require("../../models/Produto");
const faker = require('faker-br');

const seedProduto = async function () {

    const produtos = [];
  
    for (let i = 0; i < 10; i++) {
      produtos.push({
        quantidade: faker.random.number(),
        descrição: faker.lorem.text(),
        preço: faker.random.float({ precision: 0.01 }),
        título: faker.name.firstName(),
        categoria: faker.name.firstName()
      });
    }
  
    try {
      await Produto.sync({ force: true });
      await Produto.bulkCreate(produtos);
  
    } catch (err) { console.log(err); }
  }
  
  module.exports = seedProduto;