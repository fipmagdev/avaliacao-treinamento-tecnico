# Squad-Da-Vinci

## Conceito
Para o trabalho final do TT 2022.1 da EJCM, o Squad Da Vinci produziu um app de Marketplace voltado para o comércio de 
peças de computadores usadas, seminovas ou, até mesmo, novas e prontas para uso: o **Reus-e**. 
Seu objetivo é atender às necessidades tecnológicas do público jovem-adulto e, ao mesmo tempo, estimular a preservação
do meio ambiente, uma vez que relançar produtos usados no mercado reduz o desperdício. 

Nesse Marketplace, você pode acessar como visitante ou logar na sua conta, a diferença entre ambos é que é necessário
possuir uma conta para possuir acesso às várias funconalidades do app.

Logo após o Login, o usuário pode tanto anunciar produtos quanto realizar compras. 
Para anunciar, basta acessar a aba de 'Criar Anúncio' e preencher o formulário e para realizar compras basta adicionar os
produtos ao carrinho e finalizar seu pedido.
Uma vez o pedido finalizado, será gerado um e-mail ao vendedor e as partes discutem mais detalhes do negócio à parte.

Na HomePage do aplicativo, é possivel filtrar por diversas opções de peças, tanto por uma barra de pesquisa quanto por cards
de categoria como, por exemplo, selecionar um card de SSD para facilitar a busca e deixar a experiência ser algo mais fluido.

Além disso, é possível favoritar, comentar e avaliar os produtos e anunciantes da plataforma, de forma a transparecer
informações e trazer tranquilidade aos compradores que estarão utilizando a plataforma.

## Tecnologias Utilizadas
A linguagem de programação utlizada nesse projeto é o Javascript. Para a geração dos ambientes de execução, foi utilizado o
React Native no front-end da aplicação e o Node.js no back-end e utilizando a ORM do sequeliza para comunicação com o banco de dados. E, por fim, a tecnologia utilizada para armazenar nosso banco de dados foi o sqlite.

É importante ressaltar que os instaladores de pacotes são diferentes entre as partes da aplicação.
Enquanto no front-end, os pacotes são instalados através do **yarn**, o back-end utiliza **npm** para suas instalações. É de
extrema importância não confundir ambos para evitar conflitos de resoluções no git.

Para mais informações sobre as dependências utilizadas e suas versões para gerar o app, 
acesse as pastas do projeto [back/package.json](https://gitlab.com/ejcm/squad-da-vinci/-/blob/developer/back/package.json)
(para os pacotes do back-end) e [front/package.json](https://gitlab.com/ejcm/squad-da-vinci/-/blob/developer/front/package.json) (para os pacotes do front-end).

## Links Importantes
Seguem abaixo, links dos softwares utilizados para auxiliar no desenvolvimento desse projeto:

- [ ] [Trello](https://trello.com/b/vpCClg9w/squad-da-vinci)
- [ ] [Miro](https://miro.com/app/board/uXjVO-XUNGQ=/) 
- [ ] [Figma](https://www.figma.com/file/akqslJ0JExBVnmWwQh7dwH/Squad-da-Vinci?node-id=0%3A1)

## Autores do Projeto
- Gerente: Bruno Pavese
- Tech-Lead: Guilherme Matera
- Front-End: Filipe Magalhães
- Front-End: Matheus Percine
- Back-End: Christian Lorran

## Status do Projeto
Por conta do prazo final do projeto ter chegado ao fim, o projeto está encerado. Atualmente, existem algumas pendências quanto a
integraçãode funcionalidades e criação de funções mais específicas no back-end.
